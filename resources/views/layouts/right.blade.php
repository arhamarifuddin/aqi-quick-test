<aside class="control-sidebar control-sidebar-dark">
	<!-- Create the tabs -->
	<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		<li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-calendar-plus-o"></i></a></li>
		<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
	</ul>
	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="control-sidebar-home-tab">
			<h3 class="control-sidebar-heading">Period Activated</h3>
			<ul class="control-sidebar-menu">
				<li>
					<a href="javascript::;">
						<i class="menu-icon fa fa-calendar-check-o bg-red"></i>
						<div class="menu-info">
							<h4 class="control-sidebar-subheading">{{ $ops->year }}</h4>
							<p>{{ Title(Bulan($ops->month)) }}</p>
						</div>
					</a>
				</li>
			</ul>
		</div>
		
		<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
		<div class="tab-pane" id="control-sidebar-settings-tab">
			<form method="post">
				<h3 class="control-sidebar-heading">General Settings</h3>
				<div class="form-group">
					<label class="control-sidebar-subheading">
						Report panel usage
						<input type="checkbox" class="pull-right" checked>
					</label>
					<p>
						Informasi untuk setting umum
					</p>
			</form>
		</div>
	</div>
</aside>

<div class="control-sidebar-bg"></div> <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->