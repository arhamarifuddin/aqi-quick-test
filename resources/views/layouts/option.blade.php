@extends('layouts.app')

@section('title', $page)

@section('content')
<div class="content-wrapper">
	@include('layouts.header')
	<section class="content">
		<div class="row" id="info-notif">
			@if (\Session::has('success'))
			<div class="col-xs-6">
				<div class="callout callout-success">
					<h4>Notifikasi</h4>
					<p>{{ \Session::get('success') }}</p>
				</div>
			</div>
			@endif
			@if ($errors->any())
			<div class="col-xs-6">
				<div class="callout callout-warning">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
			@endif
		</div>
	
		<div class="row">
			<div class="col-xs-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h4 class="box-title">Options</h4>
						<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<form class="form-horizontal" method="post" action="{{ action('OptionController@update', $item->id) }}">
					{{csrf_field()}}
						<input name="_method" type="hidden" value="PATCH">
						<div class="box-body">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-3 control-label">Bulan</label>
									<div class="col-sm-9">
										<select class="form-control" name="month">
											@foreach($months as $key => $month)
											<option value="{{$key}}" @if($ops->month == $key) selected @endif>{{ Title($month) }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tahun</label>
									<div class="col-sm-9">
										<select class="form-control" name="year">
											@for ($i = $item->year-5; $i < ($item->year); $i++)
											<option value="{{$i}}">{{$i}}</option>
	  										@endfor
											@for ($i = $item->year; $i < ($item->year+2); $i++)
											<option value="{{$i}}" @if($item->year == $i) selected @endif>{{$i}}</option>
	  										@endfor
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" class="btn bg-orange btn-flat pull-right"><i class="fa fa-check"></i> Simpan/Update</button>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</section>
</div>
@endsection

@section('extrajs')
<script type="text/javascript">
	$('#info-notif').fadeOut(3500);
</script>
@endsection