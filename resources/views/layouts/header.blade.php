<section class="content-header">
	<h1>
		@if ($page)
		{{ $page }}
		@else
		Dashboard <small>You are logged in</small>
		@endif
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		@if($page)
		<li><a href="{{ url($link) }}">{{ $page }}</a></li>
		<li class="active">{{ $title }}</li>
		@endif
	</ol>
</section>