<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<div class="user-panel">
		</div>
		<ul class="sidebar-menu">
			<li class="header">MENU NAVIGASI</li>
			<li class="@if($link == '') active @endif">
				<a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-circle-o pull-right"></i></a>
			</li>
			<li class="treeview active">
				<a href="#"><i class="fa fa-files-o"></i><span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">
					<li><a href="{{ url('book') }}"><i class="fa fa-circle-o"></i> Buku</a></li>
					<li><a href="{{ url('cat') }}"><i class="fa fa-circle-o"></i> Kategori</a></li>
					<li><a href="{{ url('pub') }}"><i class="fa fa-circle-o"></i> Publisher</a></li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>