<!DOCTYPE html>
<html lang="{{ app()->getLocale()  }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="title" content="Imigrasi-DIY">
	<meta name="keywords" content="Kantor Imigrasi, D.I.Yogyakarta, Sistem Informasi, Yogyakarta, StockOpname">
	<meta name="description" content="Sistem Informasi Pertanian Dinas Pertanian D.I.Yogyakarta">
	<meta name="author" content="Distan">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title') | {{ config('app.name', 'App Name') }}</title>
	<link rel="shortcut icon" href="{{ asset('images/logo-sm.png') }}" type="image/png">
	<link rel="stylesheet" href="{{ asset('css/plugins/bootstrap.min.css') }}"> <!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ asset('css/plugins/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/ionicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/blue.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/morris.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/jquery-jvectormap-1.2.2.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/datepicker3.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/daterangepicker-bs3.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/bootstrap3-wysihtml5.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/jquery.dataTables.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/buttons.dataTables.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/plugins/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,600" type="text/css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
@guest
<style>
	#app{ height: 100vh; background-image: url('{{ asset('images/sleman.jpg') }}'); background-repeat: no-repeat; }
</style>
<body>
@else
<body class="hold-transition skin-blue sidebar-mini">
@endguest

	<script src="{{ asset('js/plugins/jQuery-2.1.4.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery-ui.min.js') }}"></script> <!-- https://code.jquery.com/ui/1.11.4/jquery-ui.min.js -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button); //Resolve conflict in jQuery UI tooltip with Bootstrap tooltip
	</script>
	<script src="{{ asset('js/plugins/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/plugins/raphael-min.js') }}"></script>
	<script src="{{ asset('js/plugins/morris.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery.sparkline.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery-jvectormap-1.2.2.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery.knob.js') }}"></script>
	<script src="{{ asset('js/plugins/moment.min.js') }}"></script>
	<script src="{{ asset('js/plugins/daterangepicker.js') }}"></script>
	<script src="{{ asset('js/plugins/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('js/plugins/bootstrap3-wysihtml5.all.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('js/plugins/fastclick.min.js') }}"></script>
	<script src="{{ asset('js/plugins/select2.full.min.js') }}"></script>
	<script src="{{ asset('js/dist/app.min.js') }}"></script>
	<script src="{{ asset('js/dist/dashboard.js') }}"></script>
	<script src="{{ asset('js/dist/demo.js') }}"></script>

	<script src="{{ asset('js/plugins/jquery.datatables.min.js') }}"></script>
	<script src="{{ asset('js/plugins/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('js/plugins/buttons.print.min.js') }}"></script>
	<script src="{{ asset('js/plugins/buttons.colVis.min.js') }}"></script>
	<script src="{{ asset('js/plugins/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('js/plugins/buttons.flash.min.js') }}"></script>
	<script src="{{ asset('js/plugins/jszip.min.js') }}"></script>
	<script src="{{ asset('js/plugins/pdfmake.min.js') }}"></script>
	<script src="{{ asset('js/plugins/vfs_fonts.js') }}"></script>
	<script>
		function getStock(stock, rasio, parent, child){
	    	var result = parseInt((stock / rasio), 0);
	    	var mod = stock % rasio;
	    	var tip = (stock === 0) ? parent : (result === 0 && mod !== 0) ? mod + child : result !== 0 && mod === 0 ? parent : parent + ' & ' + mod + child;
	    	return tip;
	    }
	</script>

	<div id="app" class="wrapper">
		@auth
		@include('layouts.topbar')
		@include('layouts.left')
		@endauth

		@yield('content')

		@auth
		@include('layouts.footer')
		@include('layouts.right')
		@endauth

	</div>
	
	@yield('extrajs')
</body>

</html>