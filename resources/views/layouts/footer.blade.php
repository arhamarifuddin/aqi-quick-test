<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<small>Version {{ $versi }}</small>
	</div>
	<small>Provided by &copy; 2018-2019 | <a href="http://jogja.imigrasi.go.id">Kantor IMIGRASI</a> - D.I.Yogyakarta.</small>
</footer>