@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
	<div class="content-wrapper">
		@include('layouts.header')
		<!-- Main content -->

		<section class="content">
			<div class="row">
				<section class="col-lg-5 connectedSortable">
					<div class="box box-solid bg-maroon-gradient">
						<div class="box-header">
							<i class="fa fa-calendar"></i>
							<h3 class="box-title">Kalender</h3>
							<div class="pull-right box-tools">
								<button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
								<button class="btn btn-danger btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body no-padding">
							<div id="calendar" style="width: 100%"></div>
						</div>
					</div>
				</section>
			</div>

			<div class="row">
				<section class="col-lg-7 connectedSortable hidden">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs pull-right">
							<li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
							<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
							<li class="pull-left header"><i class="fa fa-inbox"></i> Komoditas Chart</li>
						</ul>
						<div class="tab-content no-padding">
							<div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
							<div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
						</div>
					</div>
				</section>
			</div>
		</section>
	</div>
@endsection

@section('extrajs')
<script type="text/javascript">
	console.log('you are in Dashboard')
</script>
@endsection