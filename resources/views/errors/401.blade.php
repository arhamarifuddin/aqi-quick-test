@extends('layouts.app')

@section('title', $page)

@section('content')
<div class="content-wrapper">
	@include('layouts.header')

	<section class="content">
		<div class="error-page">
			<h2 class="headline text-green"> 401</h2>
			<div class="error-content">
			<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not authorized.</h3>
			<p>
				Halaman ini tidak termasuk dalam area user level yang anda gunakan.
				Hubungi dan/atau kontak administrator <br>atau silahkan <a href="{{ route('dashboard') }}">kembali ke dashboard</a> <!-- or try using the search form. -->
				<br><b>Terimakasih.</b>

			</p>
			<!-- <form class="search-form">
				<div class="input-group">
				<input type="text" name="search" class="form-control" placeholder="Search">
				<div class="input-group-btn">
					<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
				</div>
				</div>
			</form> -->
			</div>
		</div>
	</section>

</div>
@endsection