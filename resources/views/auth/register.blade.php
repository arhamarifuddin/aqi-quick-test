@extends('layouts.app')

@section('title', $page)

@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register User Baru</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('adduser') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-xs-4 control-label">Sebagai</label>
                                <div class="col-xs-6">
                                    <select class="form-control" name="user_id" required style="width:100%">
                                        <option value="3">User</option>
                                        <option value="2">Admin</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-xs-4 control-label">Nama</label>
                                <div class="col-xs-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="uname" class="col-xs-4 control-label">Username</label>
                                <div class="col-xs-6">
                                    <input id="uname" type="text" class="form-control" name="uname" value="{{ old('uname') }}" required autofocus>
                                    @if ($errors->has('uname'))
                                        <span class="help-block"><strong>{{ $errors->first('uname') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-xs-4 control-label">Password</label>
                                <div class="col-xs-6">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password-confirm" class="col-xs-4 control-label">Ulangi Password</label>
                                <div class="col-xs-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6 col-xs-offset-4">
                                    <button type="submit" class="btn bg-maroon btn-flat">Tambah User</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection