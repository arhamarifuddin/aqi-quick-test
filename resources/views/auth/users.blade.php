@extends('layouts.app')

@section('title', $page)

@section('content')
@php $counter = 1; @endphp
<div class="content-wrapper">
	@include('layouts.header')
	<section class="content">
		<div class="row">
			<!-- NOTIF -->
			@if (\Session::has('success'))
			<div class="col-xs-6 pull-right info-notif">
				<div class="callout callout-success">
					<h4>Notifikasi</h4>
					<p>{{ \Session::get('success') }}</p>
				</div>
			</div>
			@endif
			@if ($errors->any())
			<div class="col-xs-6 pull-right">
				<div class="callout callout-warning">
					<h4>Perhatian</h4>
					<ul>
						@foreach ($errors->all() as $error)
						<li><small>{{ $error }}</small></li>
						@endforeach
					</ul>
				</div>
			</div>
			@endif
			<!-- LIST -->
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">{{ $title }}</h3>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover" id="table-user">
							<thead>
							<tr>
								<th width="20">#</th>
								<th>Nama</th>
								<th>Username</th>
								<th>Email</th>
								<th>Last Token</th>
								<th>Dibuat</th>
								<th>Update</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
							</thead>
							<tbody>
							@foreach($items as $key => $item) 
								<tr class="item-{{ $item->id }}">
									<td class="center"><small>{{ $counter }}</small></td>
									<td><small>{{ strtoupper($item->name) }}</small></td>
									<td><small><code>{{ $item->uname }}</code></small></td>
									<td><small>{{ $item->email }}</small></td>
									<td><small>{{ $item->remember_token ? 'login' : '' }}</small></td>
									<td><small>{{ $item->created_at }}</small></td>
									<td><small>{{ $item->updated_at }}</small></td>
									<td width="30">
										<a href="{{ action('OptionController@edit', $item->uname) }}"><i class="fa fa-eye"></i></a>
									</td>
									<td width="30">
										<form action="{{ action('OptionController@destroy', $item->id) }}" method="post">
											{{csrf_field()}}
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-xs" type="submit"><i class="fa fa-trash"></i></button>
										</form>
									</td>
								</tr>
								@php $counter++ @endphp
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('extrajs')
<script type="text/javascript">
	$('.info-notif').fadeOut(10000);
	$('#table-user').dataTable({
		'paging': true,
		'info' : false,
		'searching': true,
		'autoWidth': false,
		// 'ordering': false,
		'order': [[ 1, 'asc' ]],
		dom: 'lBfrtip',
		buttons: [
			{
				extend: 'copyHtml5',
				text: '<i class="fa fa-copy"></i>',
				titleAttr: 'Copy data'
			},
			{
				extend: 'csvHtml5',
				text: '<i class="fa fa-file-text-o"></i>',
				titleAttr: 'export CSV'
			},
			{
				extend:'excelHtml5',
				text: '<i class="fa fa-file-excel-o"></i>',
				titleAttr: 'export Excel'
			},
			{
				extend: 'pdfHtml5',
				text: '<i class="fa fa-file-pdf-o"></i>',
				titleAttr: 'export PDF'
			},
			{
				extend: 'print',
				text: '<i class="fa fa-print"></i>',
				titleAttr: 'Print data'
			}
		],
		'lengthMenu': [[30, 50, 100, 25, 10, -1], [30, 50, 100, 25, 10, 'Semua']],
		language : { sLengthMenu: 'Show _MENU_' }
	});
</script>
@endsection