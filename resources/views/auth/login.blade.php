@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="login-box">
	<div class="login-box-body">
		<form method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}

			<div class="form-group has-feedback">
				<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="pengguna/email" required autofocus>
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				
				@if ($errors->has('email'))
					<span class="help-block">{{ $errors->first('email') }}</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<input id="password" type="password" class="form-control" name="password" placeholder="sandi/kunci" required>
				<span class="glyphicon glyphicon-asterisk form-control-feedback"></span>

				@if ($errors->has('password'))
					<span class="help-block">{{ $errors->first('password') }}</span>
				@endif
			</div>

			<div class="row">
				<div class="col-xs-6">
					<a href="/" class="btn btn-block btn-flat btn-default pull-left"><span class="glyphicon glyphicon-chevron-left"></span> Beranda</a>
				</div>
				<div class="col-xs-6">
					<button type="submit" class="btn bg-maroon btn-block btn-flat">Masuk <span class="glyphicon glyphicon-chevron-right"></span></button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
