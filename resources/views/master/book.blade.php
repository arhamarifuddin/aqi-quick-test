@extends('layouts.app')

@section('title', $page)

@section('content')
@php $counter = 1; @endphp
<div class="content-wrapper">
	@include('layouts.header')
	<section class="content">
		<div class="row">
			<!-- INPUT -->
			<div class="col-xs-6">
				<div class="box box-warning @if( $items->count() > 0 && !$errors->any() ) collapsed-box @endif">
					<div class="box-header with-border">
						<h4 class="box-title" title="klik tombol plus di samping"><i class="fa fa-clone"></i> Item</h4>
						<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse" title="tambah data"><i class="fa @if( $items->count() > 0 ) fa-plus @else fa-minus @endif"></i></button>
						</div>
					</div>
					<form class="form-horizontal" method="post" action="{{ action('BookController@store') }}">
					{{csrf_field()}}
						<div class="box-body bg-gray">
							<div class="col-sm-8 col-sm-offset-2">
								<div class="form-group {{ $errors->has('title') ? 'has-warning' : ''}}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-flag"></i></span>
										<input type="text" class="form-control" name="title" placeholder="nama buku" value="{{ old('title') }}" required>
									</div>
								</div>
								<div class="form-group {{ $errors->has('desc') ? 'has-warning' : ''}}">
									<textarea class="form-control" name="desc" rows="2" placeholder="Deskripsi">{{ old('desc') }}</textarea>
								</div>
								<div class="form-group {{ $errors->has('stock') ? 'has-warning' : ''}}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-tag"></i></span>
										<input type="number" class="form-control" name="stock" placeholder="Stok" value="{{ old('stock') ? old('stock') : 0 }}">
									</div>
								</div>
								<div class="form-group {{ $errors->has('price') ? 'has-warning' : ''}}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-tag"></i></span>
										<input type="number" class="form-control" name="price" placeholder="Harga" value="{{ old('price') ? old('price') : 0 }}">
									</div>
								</div>
								<div class="form-group {{ $errors->has('cats') ? 'has-warning' : ''}}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-flag"></i></span>
										<input type="text" class="form-control" name="cats" placeholder="Kategori" value="{{ old('cats') }}">
									</div>
								</div>
								<div class="form-group {{ $errors->has('keys') ? 'has-warning' : ''}}">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-flag"></i></span>
										<input type="text" class="form-control" name="keys" placeholder="Keywords" value="{{ old('keys') }}">
									</div>
								</div>
								<div class="form-group">
									<select class="form-control select2" name="publisher_id" required style="width:100%">
										<option class="hidden" value="">pilih publisher</option>
										@foreach($pubs as $sat)
										<option value="{{$sat->id}}" @if(old('publisher_id') == $sat->id) selected @endif>
											{{ $sat->name }}
										</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<div class="col-sm-6">
								<button type="reset" class="btn btn-default btn-flat"><i class="fa fa-square"></i> Reset</button>
							</div>
							<div class="col-sm-6">
								<button type="submit" class="btn bg-orange btn-flat pull-right"><i class="fa fa-check"></i> Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- NOTIF -->
			@if (\Session::has('success'))
			<div class="col-xs-6 pull-right info-notif">
				<div class="callout callout-success">
					<h4>Notifikasi</h4>
					<p>{{ \Session::get('success') }}</p>
				</div>
			</div>
			@endif
			@if ($errors->any())
			<div class="col-xs-6 pull-right">
				<div class="callout callout-warning">
					<h4>Perhatian</h4>
					<ul>
						@foreach ($errors->all() as $error)
						<li><small>{{ $error }}</small></li>
						@endforeach
					</ul>
				</div>
			</div>
			@endif
			<!-- LIST -->
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">{{ $title }}</h3>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th width="20">No.</th>
								<th>Judul Buku</th>
								<th>Description</th>
								<th>Kategori</th>
								<th>Keywords</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Penerbit</th>
								<th>&nbsp;</th>
							</tr>

							@foreach($items as $key => $item) 
								<tr class="item-{{ $item->id }}">
									<td class="center"><small>{{ $counter }}</small></td>
									<td><small>{{ Title($item->title) }}</small></td>
									<td><small>{{ $item->desc }}</small></td>
									<td><small>{{ '-' }}</small></td>
									<td><small>{{ '-' }}</small></td>
									<td><small>{{ $item->price }}</small></td>
									<td><small>{{ $item->stock }}</small></td>
									<td><small>{{ $item->Pub->name }}</small></td>
									<td width="30">
										<a href="{{ action('BookController@show', $item->id) }}">View</a>
										<a href="{{ action('BookController@edit', $item->id) }}">Edit</a>
										<form action="{{ action('BookController@destroy', $item->id) }}" method="post">
											{{csrf_field()}}
											<input name="_method" type="hidden" value="DELETE">
											<button class="btn btn-xs" type="submit">Delete</button>
										</form>
									</td>
								</tr>
								@php $counter++ @endphp
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('extrajs')
<script type="text/javascript">
	$('.select2').select2();
	$('.info-notif').fadeOut(10000);
</script>
@endsection