@extends('layouts.app')

@section('title', 'Home')

@section('content')
<div class="lockscreen-wrapper">
	<div class="login-logo white">Aplikasi<br>AQI</div>
	@if (Route::has('login'))
	<div class="text-center">
		@auth
			<a href="{{ route('dashboard') }}">Dasboard</a>
		@else
			<a href="{{ route('login') }}" class="btn bg-maroon">Start</a>
		@endauth
	</div>
	@endif
</div>
@endsection