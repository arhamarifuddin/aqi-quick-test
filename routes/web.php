<?php

Route::get('/', function () {
    return Auth::check() ? redirect()->route('dashboard') : view('welcome');
});

Auth::routes();

Route::get('dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');

// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('adduser');
// Route::post('register', 'Auth\RegisterController@register');

Route::resource('option','OptionController')->middleware('auth');

Route::resource('book','BookController')->middleware('auth');
Route::resource('cat','CategoryController')->middleware('auth');
Route::resource('pubs','PublisherController')->middleware('auth');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');