<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Option;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //https://laravel.com/docs/5.5/migrations#creating-indexes #laravel-5-4-key-too-long-error
        Schema::defaultStringLength(191);

        view()->share([
            'ops'       => Schema::hasTable('options') ? Option::first() : [],
            'tahun'     => date('Y'),
            'versi'     => '1.1.x',
            'page'      => 'Application',
            'link'      => '',
            'title'     => ''
            ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
