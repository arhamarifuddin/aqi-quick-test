<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
	protected $table = 'options';

	protected $fillable = ['year', 'month'];

	public function year()
	{
		return $this->belongsTo('App\option', 'year')->first();
	}

	public function month()
	{
		return $this->belongsTo('App\option', 'month')->first();
	}
}
