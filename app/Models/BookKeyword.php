<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookKeyword extends Model
{
	protected $table			= 'book_keywords';
	protected $guarded			= ['id'];
	public    $timestamps		= true;
}