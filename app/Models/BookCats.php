<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookCats extends Model
{
	protected $table			= 'book_categories';
	protected $guarded			= ['id'];
	public    $timestamps		= true;
}
