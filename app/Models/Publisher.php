<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
	use SoftDeletes;
	protected $table			= 'publishers';
	protected $guarded			= ['id'];
	public    $timestamps		= true;
	protected $dates 			= ['deleted_at'];
}
