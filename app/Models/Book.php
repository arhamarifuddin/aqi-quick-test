<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
	use SoftDeletes;
	protected $table			= 'books';
	protected $guarded			= ['id'];
	public    $timestamps		= true;
	protected $dates 			= ['deleted_at'];

	public function Pub() {
		return $this->belongsTo('App\Models\Publisher', 'publisher_id', 'id');
	}
}