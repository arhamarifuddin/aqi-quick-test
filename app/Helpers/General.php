<?php

	function Currency ($value)
	{
		$temp_val = number_format($value,0,'.','.');
		return $temp_val;
	}

	function Title ($string)
	{
		return ucfirst($string);
	}

	function Upp ($string)
	{
		return strtoupper($string);
	}

	function Low ($string)
	{
		return strtolower($string);
	}

	// lcfirst() - converts the first character of a string to lowercase.

	function Year ($data){
		$year = date("Y", strtotime($data));
		return $year;
	}

	function Month ($data){
		$month = date("m", strtotime($data));
		return $month;
	}

	function MonthName ($data){
		$month = date("M", strtotime($data));
		return $month;
	}

	function BulanDate ($data){
		$months = [1 => 'januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','nopember','desember'];
		$month = date("m", strtotime($data));
		return $months[$month];
	}

	function Bulan ($num){
		$months = [1 => 'januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','nopember','desember'];
		return $months[$num];
	}
?>