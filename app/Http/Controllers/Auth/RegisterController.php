<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $msgvalidation = [
        'name.max'        => 'Nama lengkap maks. :max karakter.',
        'uname.unique'    => 'username ini sudah terpakai.',
        'uname.nospace'   => 'username tanpa spasi',
        'uname.max'       => 'username maks. :max karakter.',
        'email.email'     => 'format email salah, sample : user@admin.co',
        'email.unique'    => 'username sudah terpakai.',
        'password.min'    => 'Password min. :min karakter',
        'password.confirmed'=> 'Pengisian ulangi password tidak sesuai',
    ];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        Validator::extend('nospace', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });

        return Validator::make($data, [
            'name'      => 'required|string|max:255',
            'uname'     => 'required|unique:users|nospace|max:150',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ], $this->msgvalidation);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // return User::create([
        $newuser = User::create([
                    'name'      => $data['name'],
                    'uname'     => $data['uname'],
                    'email'     => $data['email'],
                    'password'  => bcrypt($data['password']),
                    ]);

        //$user->roles()->attach(Role::where('role', 3)->first());
        $newuser->roles()->attach(Role::find($data['user_id']));

        // return $newuser;
        return Auth::user();
    }
}
