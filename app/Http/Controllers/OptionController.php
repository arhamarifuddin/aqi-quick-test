<?php

namespace App\Http\Controllers;

use App\Models\Option, App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller
{
    protected function auth(){
      return Auth::user()->id;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles(['admin','user']);
        // $request->user()->authorizeRoles(['admin']);

        $data['page'] = 'Settings';
        $data['title'] = 'Setting';
        $data['link'] = 'option';
        $data['months'] = [1 => 'januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','nopember','desember'];

        $datas = Option::first();
        $data['item'] = $datas;

        return view('layouts.option', $data);
    }

    public function permit(Request $request)
    {
        $data['page'] = 'Settings';
        $data['title'] = 'Setting';
        $data['link'] = 'option';

        // $datas = Option::first();
        // $data['item'] = $datas;

        return view('layouts.permits', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit($id) //change password used
    {
        $data['page'] = 'Users';
        $data['title'] = 'Ganti Password';
        $data['link'] = 'option';

        $datas = User::find(User::where('uname', $id)->first()->id);
        $data['item'] = $datas;

        return view('auth.change', $data);
    }

    protected $msgvalidation = [
        'password.min'      => 'Password min. :min karakter',
        'password.confirmed'=> 'Pengisian ulangi password tidak sesuai',
    ];

    public function updatep(Request $request, $id)
    {
        // dd($request->all());
        $data = User::find(User::where('uname', $id)->first()->id);

        $this->validate(request(), [
          'password'  => 'required|string|min:6|confirmed',
        ], $this->msgvalidation);

        $data->password = bcrypt($request->get('password'));
        $data->save();

        return redirect('option/userlist')->with('success', 'Passsword '.$id.' berhasil diperbaharui');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Option::first();

        $this->validate(request(), [
          'month' => 'integer',
          'year' => 'string|max:4'
        ]);

        $data->month = $request->get('month');
        $data->year = $request->get('year');
        $data->save();

        return back()->with('success', 'Setting berhasil diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        // $data->delete();
        $data->status = 1;
        $data->save();

        return redirect()->back()->with('success', 'User '.$data->name.', berhasil dihapus');
    }

    public function showusers()
    {
        $data['page'] = 'Users';
        $data['title'] = 'Daftar User';
        $data['link'] = 'option';

        $datas = User::where('status', 0)
                            ->where('uname', '<>', 'developer')
                            ->where('id', '<>', $this->auth())
                            ->get()
                            ->sortByDesc('updated_at')
                            ->sortByDesc('name');

        $data['items'] = $datas;

        return view('auth.users', $data);
    }
}
