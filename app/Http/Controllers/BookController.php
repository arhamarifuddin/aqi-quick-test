<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Book;
use App\Models\Publisher;
use App\Models\Category;
use App\Models\Keyword;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page'] = 'Buku';
        $data['title'] = 'Daftar';
        $data['link'] = 'book';

        $datas = Book::all()->sortByDesc('create_at');
        $data['items'] = $datas;

        $pubs = Publisher::all();
        $data['pubs'] = $pubs;

        return view('master.book', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected $msgvalidation = [
        'required'          => 'Wajib diisi',
        'string'            => 'Symbol tidak diperbolehkan',
        'alpha'             => 'Gunakan hanya alphabet',
        'alphaspace'        => 'Gunakan hanya alphabet dan spasi',
        'nospace'           => 'Tidak Gunakan spasi',
        'numeric'           => 'Gunakan hanya numeric',
        'max'               => 'Input maks. :max karakter',
    ];

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::extend('nospace', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });

        Validator::extend('alphaspace', function($attr, $value){
            return preg_match('/^[\pL\s\-\.]+$/u', $value);
        });

        $data = Validator::make($request->all(), [
            'title'         => 'required|string|min:6|max:100',
            'desc'          => 'nullable|string|max:300',
            'stock'         => 'numeric',
            'price'         => 'numeric',
            'cats'          => 'nullable',
            'keys'          => 'nullable',
            'publisher_id'  => 'required',
        ], $this->msgvalidation);

        if ($data->fails()) {
            return redirect()->back()->withErrors($data)->withInput($request->all());
        }

        Book::create([
            'title'          => $request->title,
            'desc'           => $request->desc,
            'stock'          => $request->stock,
            'price'          => $request->price,
            'publisher_id'   => $request->publisher_id,
        ]);

        return redirect()->back()->with('success', $request->title.' telah ditambahkan sebagai buku baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
