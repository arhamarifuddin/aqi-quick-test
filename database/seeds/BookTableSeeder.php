<?php

use Illuminate\Database\Seeder;
use App\Models\Book;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new Book();
	    $seed->title = 'BUKU PINTAR';
	    $seed->desc = 'deskripsi buku pintar ...... deskripsi buku pintar ';
	    $seed->publisher_id = 1;
	    $seed->save();

	    $seed = new Book();
	    $seed->title = 'JOGJA BUKU';
	    $seed->desc = 'deskripsi buku jogja pintar ...... deskripsi buku jogja';
	    $seed->publisher_id = 1;
	    $seed->save();

	    $seed = new Book();
	    $seed->title = 'BUKU MAJU';
	    $seed->desc = 'deskripsi buku maju ...maju... deskripsi buku maju ';
	    $seed->publisher_id = 3;
	    $seed->save();
    }
}
