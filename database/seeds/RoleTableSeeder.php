<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role0 = new Role();
        $role0->name = 'developer';
        $role0->description = 'Admin Developer';
        $role0->save();

	    $role1 = new Role();
	    $role1->name = 'admin';
	    $role1->description = 'Administrator of Users';
	    $role1->save();
	    
        $role2 = new Role();
	    $role2->name = 'user';
	    $role2->description = 'User as contributor';
	    $role2->save();

    }
}
