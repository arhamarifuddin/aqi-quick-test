<?php

use Illuminate\Database\Seeder;
use App\Models\Publisher;

class PublisherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new Publisher();
	    $seed->name = 'Penerbit A';
	    $seed->save();

        $seed = new Publisher();
        $seed->name = 'Penerbit B';
        $seed->save();

        $seed = new Publisher();
        $seed->name = 'Penerbit X';
        $seed->save();

        $seed = new Publisher();
        $seed->name = 'Penerbit Y';
        $seed->save();
    }
}
