<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new Option();
	    $seed->month = 10;
	    $seed->year = '2018';
	    $seed->save();
    }
}
