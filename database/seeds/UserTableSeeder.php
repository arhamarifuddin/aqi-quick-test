<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_dev  = Role::where('name', 'developer')->first();
	    $role_admin  = Role::where('name', 'admin')->first();
        $role_user = Role::where('name', 'user')->first();

	    $seed1 = new User();
	    $seed1->name = 'Admin';
	    $seed1->uname = 'admin';
	    $seed1->email = 'admin@admin.com';
	    $seed1->password = bcrypt('admin');
	    $seed1->save();
	    $seed1->roles()->attach($role_admin);
    }
}
