<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new Category();
	    $seed->name = 'Category A';
	    $seed->save();

	    $seed = new Category();
	    $seed->name = 'Category B';
	    $seed->save();

	    $seed = new Category();
	    $seed->name = 'Category C';
	    $seed->save();
    }
}
