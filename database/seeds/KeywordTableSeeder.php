<?php

use Illuminate\Database\Seeder;
use App\Models\Keyword;

class KeywordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new Keyword();
	    $seed->keyword = 'key-a';
	    $seed->save();

	    $seed = new Keyword();
	    $seed->keyword = 'key-b';
	    $seed->save();

	    $seed = new Keyword();
	    $seed->keyword = 'key-y';
	    $seed->save();

	    $seed = new Keyword();
	    $seed->keyword = 'key-x';
	    $seed->save();
    }
}
