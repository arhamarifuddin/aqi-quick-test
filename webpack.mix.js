let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .copyDirectory('resources/assets/images', 'public/images')
   .copyDirectory('resources/assets/fonts', 'public/css/fonts')
   .copyDirectory('resources/assets/plugins/datatables/images', 'public/css/images')
   .copyDirectory('resources/assets/dist/css/skins', 'public/css/skins')
   .copy([
			'resources/assets/plugins/bootstrap/css/bootstrap.min.css',
			'resources/assets/plugins/font-awesome/font-awesome.min.css',
			'resources/assets/plugins/ionicons/ionicons.min.css',
			'resources/assets/dist/css/AdminLTE.min.css',
			'resources/assets/plugins/iCheck/flat/blue.css',
			'resources/assets/plugins/morris/morris.css',
			'resources/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
			'resources/assets/plugins/datepicker/datepicker3.css',
			'resources/assets/plugins/daterangepicker/daterangepicker-bs3.css',
			'resources/assets/plugins/datatables/jquery.dataTables.min.css',
			'resources/assets/plugins/datatables/customs/buttons.dataTables.min.css',
			'resources/assets/plugins/select2/select2.min.css',
			'resources/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'
         ], 'public/css/plugins')
   .copy([
   			'resources/assets/dist/js/app.min.js',
   			'resources/assets/dist/js/pages/dashboard.js',
   			'resources/assets/dist/js/demo.js'
   		 ], 'public/js/dist')
   .copy([
   			'resources/assets/plugins/jQuery/jQuery-2.1.4.min.js',
   			'resources/assets/plugins/jQueryUI/jquery-ui.min.js',
   			'resources/assets/plugins/bootstrap/js/bootstrap.min.js',
   			'resources/assets/plugins/raphael/raphael-min.js',
			'resources/assets/plugins/morris/morris.min.js',
			'resources/assets/plugins/momentjs/moment.min.js',
			'resources/assets/plugins/sparkline/jquery.sparkline.min.js',
			'resources/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
			'resources/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
			'resources/assets/plugins/knob/jquery.knob.js',
			'resources/assets/plugins/daterangepicker/daterangepicker.js',
			'resources/assets/plugins/datepicker/bootstrap-datepicker.js',
			'resources/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
			'resources/assets/plugins/slimScroll/jquery.slimscroll.min.js',
			'resources/assets/plugins/fastclick/fastclick.min.js',
			'resources/assets/plugins/select2/select2.full.min.js',
			'resources/assets/plugins/datatables/jquery.dataTables.min.js',
			'resources/assets/plugins/datatables/customs/dataTables.buttons.min.js',
			'resources/assets/plugins/datatables/customs/buttons.print.min.js',
			'resources/assets/plugins/datatables/customs/buttons.colVis.min.js',
			'resources/assets/plugins/datatables/customs/buttons.html5.min.js',
			'resources/assets/plugins/datatables/customs/buttons.flash.min.js',
			'resources/assets/plugins/datatables/customs/jszip.min.js',
			'resources/assets/plugins/datatables/customs/pdfmake.min.js',
			'resources/assets/plugins/datatables/customs/vfs_fonts.js'
         ], 'public/js/plugins');

